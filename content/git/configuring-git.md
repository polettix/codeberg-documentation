---
eleventyNavigation:
  key: ConfiguringGit
  title: Configuring Git
  parent: Git
---

Once you managed to get Git up and running, the first thing you must do before you can use your fresh installation of Git is to tell Git your name and email address. You only have to do this once. This is easily done with `Git Bash`:

```bash
git config --global user.name 'knut'
git config --global user.email 'knut@example.com'
```
The username can be anything, but it is important that the email is the same as the one you use on Codeberg. That is because the email address will later be used to assign your commits to your account. To verify that you've set up everything properly, run:

```bash
git config --global --list
```

> If you do not wish to include your email address with your commits, you can opt to specify `USERNAME@noreply.codeberg.org` here instead, where USERNAME is your Codeberg username

If you ever want to change your name or email address, simply run the corresponding command again. You could also omit the `--global` flag to set a username and email address only for the git repository you're currently in.
